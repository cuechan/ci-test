#!/usr/bin/python3

import yaml
import subprocess
import os
import copy


ARTIFACTS_EXPIRE = "3 day"
DOCKER_BUILD_IMAGE = "registry.chaotikum.net/freifunk-luebeck/gluon-build:latest"

MAKE_FLAGS = ["--silent", "-C", "gluon", "GLUON_SITEDIR=.."]


BEFORE_SCRIPT = [
	"sleep 3"
]

VARIABLES = {
	"CCACHE_DIR": "$CI_PROJECT_DIR/ccache",
	"CCACHE_MAXSIZE": "2G",
	"FORCE_UNSAFE_CONFIGURE": "1",
}


def get_available_targets():
	return [
		"A",
		"B",
		"C",
		"D",
		"F",
		"G",
		"H",
		"I",
		"J",
		"K",
		"L",
		"M",
		"N",
		"O",
		"P",
	]


# the main ci config
ci = {
	"image": "debian:buster",
	"default": {
		"interruptible": True
	},
	"before_script": BEFORE_SCRIPT,
	"variables": {
		"GIT_SUBMODULE_STRATEGY": "recursive",
		"FORCE_UNSAFE_CONFIGURE": "1",
		**VARIABLES,
	},
	"stages": [
		"pre-build-tests",
		"build",
		"build-on-failure",
		"post build",
		"test",
		"prepare-deploy",
		"test-deploy",
		"deploy",
	]
}


for arch in get_available_targets():
	ci[f"build-{arch}"] = {
		"stage": "build",
		"needs": [],
		"image": DOCKER_BUILD_IMAGE,
		"retry": {
			"max": 2,
			"when": "runner_system_failure"
		},
		"cache": {
			"paths": ['ccache'],
			"key": "${TARGET}",
		},
		"variables": {
			"GLUON_ARCH": arch,
			"GLUON_SITEDIR": "..",
			"GLUON_DEPRECATED": 1,
			"GLUON_AUTOUPDATER_ENABLED": 1,
			"GLUON_AUTOUPDATER_BRANCH": "stable",
			"GLUON_BRANCH": "$GLUON_AUTOUPDATER_BRANCH",
			**VARIABLES,
		},
		"script": [
			"sleep 3"
		],
		"artifacts": {
			# don't add "when: always".
			# it will always skip the build-on-failure stage
			"expire_in": ARTIFACTS_EXPIRE,
			"paths": [
				"gluon/output",
				"logs"
			]
		}
	}

	# build again if pipeline failed but with verbose flags
	ci[f"build-{arch}"] = copy.deepcopy(ci[f"build-{arch}"])
	del ci[f"build-{arch}"]['needs']

	ci[f"build-{arch}"]['when'] = "on_failure"
	ci[f"build-{arch}"]['stage'] = "build-on-failure"
	ci[f"build-{arch}"]['dependencies'] = []

	# only save logs as artifacts
	ci[f"build-{arch}"]['artifacts']['paths'] = ["logs"]
	ci[f"build-{arch}"]['script'] = [
		"sleep 3"
	]


# ci['foo'] = dict()
# ci['foo']['stage'] = "build-on-failure"
# ci['foo']['when'] = "on_failure"
# ci['foo']['script'] = ["true"]



# test image names
ci['test:images'] = {
	"stage": "test",
	"needs": [f"build-{arch}" for arch in get_available_targets()],
	"allow_failure": True,
	"before_script": [
		"apt update",
		"apt install -qq -y tree"
	],
	"script": [
		"sleep 3"
	],
	"artifacts": {
		"when": "always",
		"expire_in": ARTIFACTS_EXPIRE,
		"paths": ["gluon/output"]
	}
}

ci['test:image-names'] = {
	"stage": "test",
	"needs": [f"build-{arch}" for arch in get_available_targets()],
	"allow_failure": False,
	"before_script": [
		"sleep 3"
	],
	"script": [
		"sleep 3"
	],
	"artifacts": {
		"when": "always",
		"expire_in": ARTIFACTS_EXPIRE,
		"paths": ["gluon/output"]
	}
}


ci['generate manifest'] = {
	"stage": "post build",
	"needs": [f"build-{arch}" for arch in get_available_targets()],
	"image": DOCKER_BUILD_IMAGE,
	"cache": {
		"paths": ['ccache'],
		"key": "manifest",
	},
	"variables": {
		"FORCE_UNSAFE_CONFIGURE": "1",
	},
	"script": [
		"sleep 3"
	],
	"artifacts": {
		"when": "always",
		"expire_in": ARTIFACTS_EXPIRE,
		"paths": ["gluon/output"]
	}
}


ci['sign manifest'] = {
	"stage": "prepare-deploy",
	"needs": [*[f"build-{arch}" for arch in get_available_targets()], "generate manifest"],
	"image": DOCKER_BUILD_IMAGE,
	"rules": [
		{'if': '$SIGNING_KEY'}
	],
	"cache": {
		"paths": ['ccache'],
		"key": "manifest",
	},
	"script": [
		"sleep 3"
	],
	"artifacts": {
		"when": "always",
		"expire_in": ARTIFACTS_EXPIRE,
		"paths": ["gluon/output"]
	}
}


ci['test:manifest-length'] = {
	"stage": "test-deploy",
	"needs": ["generate manifest"],
	"before_script": [],
	"script": [
		"sleep 3"
	],
	"artifacts": {
		"expire_in": ARTIFACTS_EXPIRE,
		"when": "always",
		"paths": ["gluon/output"]
	}
}

ci['test:manifest-signature'] = {
	"stage": "test-deploy",
	"needs": ["sign manifest"],
	"rules": [
		{'if': '$SIGNING_KEY'}
	],
	"image": DOCKER_BUILD_IMAGE,
	"before_script": [],
	"script": [
		"sleep 3"
	],
	"artifacts": {
		"expire_in": ARTIFACTS_EXPIRE,
		"when": "always",
		"paths": ["gluon/output"]
	}
}


# Upload Jobs
##############

ci['upload'] = {
	"stage": "deploy",
	"needs": ["generate manifest"],
	"rules": [
		{'if': '$DEPLOY_USER && $DEPLOY_HOST'}
	],
	"before_script": [
		"sleep 3"
	],
	"script": [
		"sleep 3"
	],
}

ci['upload-package-registry'] = {
	'stage': "deploy",
	"needs": ["generate manifest"],
	'script': [
		"sleep 3"
	],
	'rules': [{
		'if': '$GITLAB_CI'
	}]
}


print(yaml.dump(ci, sort_keys=False,))
# print(get_available_targets())
